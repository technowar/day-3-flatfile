var express = require('express');
var db = require('./lib/db');

var	app = express();

db.connect('./');
db.connection.on('open', function (db) {
	console.log('Connection established to database.');
});

db.connection.on('error', function (err) {
	console.log('Error on connection: ', err);
});

app.configure(function () {
	app.use(express.bodyParser());
});

var task = db.model('Task');

// Main

app.get('/', function (req, res) {
	res.writeHead(200, {'content-type':'text/html'});
	res.end(require('fs').readFileSync('./list.html'));
});

// Get

app.get('/tasks', function (req, res) {
	task.read(function (err, docs) {
		if (err) {
			res.statusCode = 500;
			res.end('Something went wrong');
		}

		res.end(JSON.stringify(docs));
	});
});

// POST

app.post('/tasks', function (req, res) {
	task.insert(req.body, function (err, result) {
		if (err) {
			res.statusCode = 505;
			res.end('Something went wrong.');
		}

		res.writeHead(200, {'content-type':'application/json'});
		res.end(JSON.stringify(result));
	});
});

// DELETE

app.del('/tasks/:id', function (req, res) {
	var item_id = parseInt(req.params.id, 10);
	var query = {_id:item_id};

	task.delete(query, function (err, results) {
		if (err) {
			res.writeHead(err.error_status, {'content-type':'application/json'});
			res.end(JSON.stringify(err));

			return;
		}

		res.writeHead(200, {'content-type':'application/json'});
		res.end(JSON.stringify(results));
	});
});

// PUT

app.put('/tasks/:id', function (req, res) {
	var item_id = parseInt(req.params.id, 10);
	var query = {
		filter : {_id:item_id}, data : req.body
	};

	task.update(query, function (err, result) {
		if (err) {
			res.writeHead(err.error_status, {'content-type':'application/json'});
			res.end(JSON.stringify(err));

			return;
		}

		res.writeHead(200, {'content-type':'application/json'});
		res.end(JSON.stringify(result));
	});
});

app.listen(9090, function () {
	console.log('App listening on localhost:9090');
});