var fs = require('fs');
var path = require('path');
var _ = require('underscore');
var EventEmitter = require('events').EventEmitter;

exports.connect = connect;
var connection = exports.connection = new EventEmitter();

function connect (location, callback) {
  var file = 'db.data';

  var exists = fs.existsSync(path.join(location, file));
  exports.location = path.join(location, file);

  process.nextTick(function () {
    if (!exists) {
      try {
        fs.writeFileSync(exports.location, JSON.stringify({}));
      } catch (err) {
        if (callback && _.isFunction(callback)) {
          callback(err);
        }
        return exports.connection.emit('error', err);
      }
    }

    if (callback && _.isFunction(callback)) {
      callback(null);
    }
    connection.emit('open', exports);
  });
}

function model(modelName) {
  var _model = new Model(modelName);
  return _model;
}

exports.model = model;

function Model(modelName) {
  this.name = modelName;
  this.dbname = modelName.toLowerCase();
  this.file = exports.location;
}

Model.prototype.getNextID = function(callback) {
    var _this = this;

    fs.readFile(this.file, {encoding: 'utf8'}, function (err, data) {
      if (err) return callback(err);
      try {
        data = JSON.parse(data);
      } catch(e) {
        return callback('JSON parse on getNextID: ' + e);
      }

      if (!data[_this.dbname]) {
        data[_this.dbname] = [];
        return callback(null, 0);
      } else {
        return callback(null, data[_this.dbname].length);
      }
    });
};

Model.prototype.insert = function(object, callback) {
  var _this = this;

  this.getNextID(function (err, id) {

    if (err) return callback(err);

    fs.readFile(_this.file, {encoding: 'utf8'}, function (err, data) {
      if (err) return callback(err);

      try {
        data = JSON.parse(data);
      } catch (e) {
        return callback('JSON.parse at insert: ' + e);
      }

      object['_id'] = id;

      if (!data[_this.dbname]) {
        data[_this.dbname] = [];
        data[_this.dbname].push(object);
      } else {
        data[_this.dbname].push(object);
      }

      fs.writeFile(_this.file, JSON.stringify(data), function (err) {
          if (err) return callback(err);

          return callback(null, object);
      });
    });
  });
};

Model.prototype.read = function(query, callback) {
  // body...

  if (_.isFunction(query)) {
    callback = query;
    this.getDocs(callback);
  } else {
    this.getDoc(query, callback);
  }
};

Model.prototype.delete = function(query, callback) {
  var _this = this;

  fs.readFile(this.file, {encoding: 'utf8'}, function(err, data) {
    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    }

    catch(e) {
      callback('JSON.parse at getDocs ' + e);
    }

    if (!_.findWhere(data[_this.dbname], query)) {
      return callback({error_status: 404, error_message: "Nothing Found."});
    }

    data[_this.dbname] = _.reject(data[_this.dbname], function(i) {
      var pairs = _.pairs(query);
      var matched_pairs = 0;

      _.each(pairs, function(pair) {
        if (pair[1] === i[pair[0]]) matched_pairs += 1;
      });

      return matched_pairs === pairs.length;
    });

    fs.writeFile(_this.file, JSON.stringify(data), function (err) {
        if (err) {
          return callback(err);
        }

        return callback(null, data[_this.dbname]);
    });
  });
};

Model.prototype.update = function(query, callback) {
  var _this = this;
  var updated_items = [];

  fs.readFile(this.file, {encoding: 'utf8'}, function(err, data) {
    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    }

    catch(e) {
      callback('JSON.parse at getDocs ' + e);
    }

    if (!_.findWhere(data[_this.dbname], query.filter)) {
      return callback({error_status: 404, error_message: "Nothing Found."});
    }

    var new_data = _.map(data[_this.dbname], function(i) {
      var filter_pairs = _.pairs(query.filter);
      var data_pairs = _.pairs(query.data);
      var matched_filter_pairs = 0;

      _.each(filter_pairs, function(fp) {
        if (fp[1] === i[fp[0]]) matched_filter_pairs += 1;
      });

      if (matched_filter_pairs === filter_pairs.length) {
        _.each(data_pairs, function(dp) {
          i[dp[0]] = dp[1];
        });

        updated_items.push(i);
      }

      return i;
    });

    data[_this.dbname] = new_data;

    fs.writeFile(_this.file, JSON.stringify(data), function (err) {
        if (err) return callback(err);
        return callback(null, updated_items);
    });
  });
};

Model.prototype.getDocs = function(callback) {
  // body...

  var _this = this;
  fs.readFile(this.file, {encoding: 'utf8'}, function(err, data) {
    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    } catch(e) {
      callback('JSON.parse at getDocs ' + e);
    }

    callback(null, data[_this.dbname]);

  });

};

Model.prototype.getDoc = function(query, callback) {
  // body...
  var _this = this;
  fs.readFile(this.file, {encoding: 'utf8'}, function (err, data) {

    if (err) return callback(err);

    try {
      data = JSON.parse(data);
    } catch(e) {
      return callback('JSON.parse at getDoc: ' + e);
    }

    var entry = _.findWhere(data[_this.dbname], query);

    callback(null, entry);
  });
};